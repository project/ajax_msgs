<?php

/**
 * @file
 * Admin page callbacks for the ajax_msgs module.
 */

/**
 * Build the admin form.
 */
function ajax_msgs_admin_form($form, &$form_state) {

  $form['ajax_msgs_element'] = array(
    '#title' => t('Element ID'),
    '#type' => 'textfield',
    '#default_value' => variable_get('ajax_msgs_element', 'messages'),
    '#description' => t('The ID of the element to use to add messages to the page. A comma separated list can be added. The first matching element found will be used.'),
  );

  $form['ajax_msgs_element_method'] = array(
    '#title' => t('Insert location'),
    '#type' => 'select',
    '#default_value' => variable_get('ajax_msgs_element_method', 'append'),
    '#options' => array(
      'html'    => t('Replace'),
      'append'  => t('Append'),
      'prepend' => t('Prepend'),
      'before'  => t('Before'),
      'after'   => t('After'),
    ),
    '#description' => t('Where should the message be added relative to the element.'),
  );

  return system_settings_form($form);

}
