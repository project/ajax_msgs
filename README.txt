-- SUMMARY --

This module stops messages ever being delivered as part of the initial request.

Instead it make an ajax call after the page has loaded to fetch the messages and display them.

This has two benefits:

1) The page is cacheable as no messages are polluting the HTML.
2) Messages never get lost in 404 requests


-- REQUIREMENTS --

Drupal 7.
Also your page needs to have a div with ID messages.


-- INSTALLATION --

* Install as usual, see http://drupal.org/node/895232 for further information.
* By default messages are appended to a div with ID messages. The ID of the div
  and the method used can be set on the settings page at
  /admin/config/user-interface/ajax-messages.
* The available methods are: Replace, Append, Prepend, Before After


-- EXAMPLE --

My main theme has a div with ID messages that is always visible. However my
admin theme only shows the messages wrapping div if there are messages to add
to the page. To get messages to show on both themes I set the element ID to:
"messages, content-wrapper" and the method to "before".
This means on the frontend theme messages are added before the div with ID
messages and on the admin theme messages are added before the div with ID
content-wrapper.
